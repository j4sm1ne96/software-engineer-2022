import React, { useState } from 'react';

import Table from './Components/Table';

const App = () => {
  const [nameList, setNameList] = useState([]);

  const nameRef = React.createRef();

  const handleSubmitClick = (data) => {
    // fill in with your logic to handle addition of names
      const updatedEmployeeList = [...nameList];
      updatedEmployeeList.push(data);
      setNameList(updatedEmployeeList);
  };

  return (
    <div>
      <input type="button" value="add" onClick={handleSubmitClick} />
      <Table names={nameList} />
    </div>
  );
}

export default App;
